*** ------------------------------- PROJECT_TEMPLATE_NAME - TO BE REMOVED ------------------------------- ***

This is a template project to create a new plugin for SurveyPad. It creates a dummy plugin that can be integrated in SurveyPad.

When you want to create a new SurveyPad plugin, you should fork this project so you directly have a project for your plugin, along with tests
and CI.

Once forked, you can do a `git grep -n PROJECT_TEMPLATE_NAME` to see all the things you have to change. Generally, you just need to change `PROJECT_TEMPLATE_NAME` with your plugin name, but in some occasion, it can be a bit more complicated (like these explanations for instance, you should remove it all).

Run the command `git grep -n PROJECT_TEMPLATE_NAME` from the root of your repository until you don't get any results anymore. Then, you can launch CMake and everything.

In the folder `source`, you have a default dummy plugin. You can start from there to develop your own plugin.

In order to test your plugin in action it should be added to SurveyPad as a Git submodule inside `plugins` directory.

*** ------------------------------- PROJECT_TEMPLATE_NAME - TO BE REMOVED ------------------------------- ***


[![pipeline status](https://gitlab.cern.ch/apc/susofts/interfaces/plugins/PROJECT_TEMPLATE_NAME/badges/master/pipeline.svg)](https://gitlab.cern.ch/apc/susofts/interfaces/plugins/PROJECT_TEMPLATE_NAME/commits/master)

PROJECT_TEMPLATE_NAME
=====================

SurveyPad plugin for PROJECT_TEMPLATE_NAME.

You can find more information about SurveyPad here: <https://gitlab.cern.ch/apc/susofts/interfaces/SurveyPad>.

You can find more information about PROJECT_TEMPLATE_NAME here: <https://gitlab.cern.ch/apc/susofts/processing/PROJECT_TEMPLATE_NAME>.

#### Table of Content ####

[Purpose](#purpose)

[Download](#download)

[Documentation](#documentation)
- [User guide](#user-guide)
- [Doxygen](#doxygen)
- [Other](#other)

[Build instructions](#build-instructions)
- [Requirements](#requirements)
- [Generate project](#generate-project)
- [Build](#build)
- [Tests](#tests)

[Contribute](#contribute)
- [Jira](#jira)
- [Pull requests](#pull-requests)
- [Automatic tests](#automatic-tests)

Purpose
-------

this plugin allows the control of PROJECT_TEMPLATE_NAME from SurveyPad. The integration of this plugin in SurveyPad allows you to open and edit input files, launch the application and read the outputs.


Download
--------

You can download the last version of the plugin for Windows or Linux from the tag page: https://gitlab.cern.ch/apc/susofts/interfaces/plugins/PROJECT_TEMPLATE_NAME/-/tags

Documentation
-------------

### User guide ###

See the documentation page of SurveyPad: https://readthedocs.web.cern.ch/display/SUS/SurveyPad+User+Guide

### Doxygen ###

The Doxygen documentation is meant for developers only. Follow the [Build instructions](#build-instructions) to set up your projects. Then you can build the `doc` target to create the Doxygen documentation. You will need [Doxygen](https://www.doxygen.nl/download.html) and [GraphViz](http://www.graphviz.org/download/#executable-packages) installed and configured.

Once built, you can open the file `build/html/index.html` as an entry point to the documentation.

### Other ###

You can find further documentation in the folder [doc](./doc). You can also find some [notes on the code](https://readthedocs.web.cern.ch/display/SUS/Notes+on+the+code).

Build instructions
------------------

Before starting, you can have a look at the documentation about [Getting started with C++](https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22153013) for the CERN survey applications.

### Requirements ###

This plugin can be built on Windows or Linux. To do so, you need at least:
- a C++17 compiler
- CMake 3.10+
- TUT

For Windows, you can follow the steps in the aforementioned [Getting started with C++](https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22153013) documentation.

For Linux, you have an example of the needed steps in the [Dockerfile](https://gitlab.cern.ch/apc/common/docker-image-susoft-cpp/-/blob/master/Dockerfile) of the [sus_ci_cppworker](https://gitlab.cern.ch/apc/common/docker-image-susoft-cpp) project (the Docker image used to automatically run the tests on GitLab-CI).
Note that the `devtoolset` trick is only necessary on the CC7 (Cern CentOS 7) as it doesn't provide a C++17 compiler by default.

### Generate project ###

We use CMake to generate projects, thus it is possible to generate projects for MSVC, Eclipse, or simple Unix makefiles. See the [CMake Generators documentation](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html) page.

To generate the project, you need first to create a subdirectory named `build/`, and then run CMake inside:

```bash
# first we download the SurveyLib and the SUGL as a submodules
$ git submodule update --init
# then we generate the project
$ mkdir build && cd build/
$ cmake -G "Visual Studio 16 2019" ../source # Use another generator here if you wish
```

### Build ###

Once generated, you can open your project in the `build/` subfolder. If you use MSVC, you can open the file `build/PROJECT_TEMPLATE_NAME.sln`.

you can see that CMake has generated several targets, among others:
- `ALL_BUILD` builds all except the doxygen documentation
- `ZERO_CHECK` reruns CMake and automatically updates your project
- `doc` builds the Doxygen documentation

### Tests ###

To build the tests, build the target `Tests` and run it. We Use TUT to generate unit tests. Note that the tests are automatically performed on Gitlab-CI for each contribution. You can see the results in the CI page.

Contribute
----------

This is a private CERN repository, thus it doesn't accept contributions from outside CERN.

To report an issue (bug, or feature request), follow the [Jira](#jira) subsection. For development, please read on.

### Jira ###

Any request, bug or development should have a Jira issue. You can create an issue on the [dedicated Jira board](https://its.cern.ch/jira/browse/SUS). This is mandatory for both the users and the developers.

### Pull requests ###

The most up-to-date stable branch is `master`. The beta branch is `appwidevs`. As stable branches, you **must not** commit directly in them. You need to create a specific branch for your on-going development and commit there. As we use CMake, if you add a file, don't forget to add it in one of the `CMakeFile.txt`!

Once you have finished your work, you should create a Pull Request (PR, or Merge Request) from your branch to `appwidevs`. The description of your PR should include a link to the corresponding task in Jira.

Once your PR has been reviewed by another developer and accepted, it can be merged into `appwidevs`. Note that, for the sake of a nice Git history, your branch needs to be up to date with `appwidevs`. If it is not the case, you will have to rebase, either automatically from GitLab if there are no conflicts, or manually otherwise.

### Automatic tests ###

Automatic tests are performed each time you push a commit. These tests include compilation of `ALL_BUILD` target, and running the `Tests` target, all on Linux 64 bits and 64 bits. If the tests don't pass, your PR will not be merged.

For each release, when `master` is updated, GitLab-CI will automatically build the installers.
