#include "PROJECT_TEMPLATE_NAME.hpp"

#include <Logger.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

#include "trinity/PROJECT_TEMPLATE_NAMEConfig.hpp"
#include "trinity/PROJECT_TEMPLATE_NAMEGraphicalWidget.hpp"
#include "trinity/PROJECT_TEMPLATE_NAMELaunchObject.hpp"

const ShareablePointsList PROJECT_TEMPLATE_NAME::_pointsTemplate = []() -> ShareablePointsList {
	ShareablePointsList spl("TEMPLATE");
	auto &frame = spl.getRootFrame();

	frame.setName("rootFrame");
	frame.add(new ShareablePoint{"ORIGIN", {0, 0, 0}, "This is a template point", "Origin"});
	frame.add(new ShareablePoint{"pointX", {0, 0, 1}, "This is a template point", "X axis"});
	frame.add(new ShareablePoint{"pointY", {0, 1, 0}, "This is a template point", "Y axis"});
	frame.add(new ShareablePoint{"pointZ", {1, 0, 0}, "This is a template point", "Z axis"});
	auto &child = frame.addFrame();
	child.setName("childFrame");
	child.setRotation({12.5, 0, 3});
	child.setScale(0.5);
	child.setTranslation({10, 10, 10});
	child.add(new ShareablePoint{"dummy1", {12, 13, 14}});
	child.add(new ShareablePoint{"dummy2", {-12, -13, -14}});

	return std::move(spl);
}();

QIcon PROJECT_TEMPLATE_NAME::icon() const
{
	return QIcon(":/icons/susoft");
}

void PROJECT_TEMPLATE_NAME::init()
{
	logDebug() << "Plugin PROJECT_TEMPLATE_NAME loaded";
}

void PROJECT_TEMPLATE_NAME::terminate()
{
	logDebug() << "Plugin PROJECT_TEMPLATE_NAME unloaded";
}

SConfigWidget *PROJECT_TEMPLATE_NAME::configInterface(QWidget *parent)
{
	logDebug() << "loading PROJECT_TEMPLATE_NAME::configInterface (PROJECT_TEMPLATE_NAMEConfig)";
	return new PROJECT_TEMPLATE_NAMEConfig(this, parent);
}

SGraphicalWidget *PROJECT_TEMPLATE_NAME::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading PROJECT_TEMPLATE_NAME::graphicalInterface (PROJECT_TEMPLATE_NAMEGraphicalWidget)";
	return new PROJECT_TEMPLATE_NAMEGraphicalWidget(this, parent);
}

SLaunchObject *PROJECT_TEMPLATE_NAME::launchInterface(QObject *parent)
{
	logDebug() << "loading PROJECT_TEMPLATE_NAME::launchInterface (PROJECT_TEMPLATE_NAMELaunchObject)";
	return new PROJECT_TEMPLATE_NAMELaunchObject(this, parent);
}

QString PROJECT_TEMPLATE_NAME::getExtensions() const noexcept
{
	return QString(tr("JSON (*.json);;CSV (*.csv);;Text file (*.txt)"));
}

const QString &PROJECT_TEMPLATE_NAME::_name()
{
	static const QString _sname = "PROJECT_TEMPLATE_NAME";
	return _sname;
}
