#include "PROJECT_TEMPLATE_NAMETableEditorWidget.hpp"

#include <Logger.hpp>
#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/table/CommandTable.hpp>
#include <editors/table/TableEditor.hpp>
#include <io/ShareablePointsListIOCSV.hpp>
#include <utils/UndoStack.hpp>

class PROJECT_TEMPLATE_NAMETableEditorWidget::_PROJECT_TEMPLATE_NAMETableEditorWidget_pimpl
{
public:
	UndoStack undoStack;
};

PROJECT_TEMPLATE_NAMETableEditorWidget::PROJECT_TEMPLATE_NAMETableEditorWidget(TableEditor *editor, SPluginInterface *owner, QWidget *parent) :
	TableEditorWidget(editor, owner, parent), _pimpl(std::make_unique<_PROJECT_TEMPLATE_NAMETableEditorWidget_pimpl>())
{
	// undo/redo
	connect(&_pimpl->undoStack, &UndoStack::hasChanged, [this](bool isUndo, bool isRedo) {
		emit undoAvailable(isUndo);
		emit redoAvailable(isRedo);
	});
	// If action done, push it to QUndoStack
	connect(this->editor(), &TableEditor::actionDone, &_pimpl->undoStack, &UndoStack::push);
}

PROJECT_TEMPLATE_NAMETableEditorWidget::~PROJECT_TEMPLATE_NAMETableEditorWidget() = default;

void PROJECT_TEMPLATE_NAMETableEditorWidget::setContent(const ShareablePointsList &spl)
{
	TableEditorWidget::setContent(spl);
	_pimpl->undoStack.reset({new CommandTable(editor())});
}

void PROJECT_TEMPLATE_NAMETableEditorWidget::undo()
{
	TableEditorWidget::undo();
	_pimpl->undoStack.undo();
}

void PROJECT_TEMPLATE_NAMETableEditorWidget::redo()
{
	TableEditorWidget::redo();
	_pimpl->undoStack.redo();
}

bool PROJECT_TEMPLATE_NAMETableEditorWidget::_save(const QString &path)
{
	IShareablePointsListIO::writeFile(path.toStdString(), ShareablePointsListIOCSV().write(getContent()));
	return true;
}

bool PROJECT_TEMPLATE_NAMETableEditorWidget::_open(const QString &path)
{
	setContent(ShareablePointsListIOCSV().read(IShareablePointsListIO::readFile(path.toStdString())));
	return true;
}
