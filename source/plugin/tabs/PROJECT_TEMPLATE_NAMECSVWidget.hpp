/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECT_TEMPLATE_NAMECSVWIDGET_HPP
#define PROJECT_TEMPLATE_NAMECSVWIDGET_HPP

#include <editors/text/TextEditorWidget.hpp>

#include "trinity/PROJECT_TEMPLATE_NAMEGraphicalWidget.hpp"

class QMimeData;

class PROJECT_TEMPLATE_NAMECSVWidget : public TextEditorWidget
{
	Q_OBJECT

public:
	PROJECT_TEMPLATE_NAMECSVWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~PROJECT_TEMPLATE_NAMECSVWidget() override = default;

	// SGraphicalWidget
	virtual ShareablePointsList getContent() const override;

public slots:
	// SGraphicalWidget
	virtual void setContent(const ShareablePointsList &spl) override;

protected:
	// SGraphicalWidget
	virtual void _newEmpty() override;

private:
	QString clipboardMimeType() const;
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);
};

#endif // PROJECT_TEMPLATE_NAMECSVWIDGET_HPP
