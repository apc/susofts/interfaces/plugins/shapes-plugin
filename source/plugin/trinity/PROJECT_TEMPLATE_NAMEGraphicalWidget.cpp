#include "PROJECT_TEMPLATE_NAMEGraphicalWidget.hpp"

#include <QFileInfo>
#include <QTabWidget>

#include "PROJECT_TEMPLATE_NAME.hpp"
#include "tabs/PROJECT_TEMPLATE_NAMECSVWidget.hpp"
#include "tabs/PROJECT_TEMPLATE_NAMEJSONWidget.hpp"
#include "tabs/PROJECT_TEMPLATE_NAMETableEditor.hpp"
#include "tabs/PROJECT_TEMPLATE_NAMETableEditorWidget.hpp"

PROJECT_TEMPLATE_NAMEGraphicalWidget::PROJECT_TEMPLATE_NAMEGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	STabInterface(owner, parent),
	_csvTab(new PROJECT_TEMPLATE_NAMECSVWidget(owner, this)),
	_jsonTab(new PROJECT_TEMPLATE_NAMEJSONWidget(owner, this)),
	_tableTab(new PROJECT_TEMPLATE_NAMETableEditorWidget(new PROJECT_TEMPLATE_NAMETableEditor(this), owner, this))
{
	_csvTab->setWindowTitle(tr("CSV editor"));
	_jsonTab->setWindowTitle(tr("JSON editor"));
	_tableTab->setWindowTitle(tr("Table editor"));
	tab()->addTab(_jsonTab, _jsonTab->windowTitle());
	tab()->addTab(_csvTab, _csvTab->windowTitle());
	tab()->addTab(_tableTab, _tableTab->windowTitle());
}

bool PROJECT_TEMPLATE_NAMEGraphicalWidget::_save(const QString &path)
{
	bool b = STabInterface::_save(path);
	setWindowTitle(QFileInfo(getPath()).baseName());
	return b;
}

bool PROJECT_TEMPLATE_NAMEGraphicalWidget::_open(const QString &path)
{
	bool b = STabInterface::_open(path);
	setWindowTitle(QFileInfo(getPath()).baseName());
	return b;
}

void PROJECT_TEMPLATE_NAMEGraphicalWidget::_newEmpty()
{
	STabInterface::_newEmpty();
	setWindowTitle("NewProject");
}
